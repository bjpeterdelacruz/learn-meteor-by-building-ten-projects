Meteor.methods({
    "insertPlan": function(plan_name, plan_label, days, price, description, is_default) {
        if (is_default == '1') {
            var defaultPlan = getDefaultPlan();
            if (defaultPlan) {
                throw new Meteor.Error("default-plan-exists", "A default plan named " + defaultPlan.plan_name + " already exists.");
            }
        }
        Plans.insert({
            plan_name: plan_name,
            plan_label: plan_label,
            days: days,
            price: price,
            description: description,
            is_default: is_default
        });
    },
    "updatePlan": function(id, plan_name, plan_label, days, price, description, is_default) {
        if (is_default == '1') {
            var defaultPlan = getDefaultPlan();
            if (defaultPlan) {
                throw new Meteor.Error("default-plan-exists", "A default plan named " + defaultPlan.plan_name + " already exists.");
            }
        }
        Plans.update({_id: id},
            {
                $set: {
                    plan_name: plan_name,
                    plan_label: plan_label,
                    days: days,
                    price: price,
                    description: description,
                    is_default: is_default
                }
            });
    },
    "removePlan": function(id) {
        Plans.remove(id);
    },
    "insertSubscriber": function(plan_info, user_id, user_email) {
        Subscribers.insert({
            plan_name: plan_info.plan_name,
            plan_label: plan_info.plan_label,
            plan_days: plan_info.days,
            plan_price: plan_info.price,
            plan_description: plan_info.description,
            user_id: user_id,
            user_email: user_email,
            join_date: new Date()
        });
    },
    "cancelPlan": function(id) {
        Subscribers.remove(id);
    }
});

function getDefaultPlan() {
    return Plans.findOne({is_default: '1'});
}