Router.configure({
    layoutTemplate: 'layout'
});

var OnBeforeActions = {
    isNotLoggedIn: function() {
        if (!Meteor.user()) {
            Router.go('/');
        } else {
            this.next();
        }
    },
    isAdmin: function() {
        if (Roles.userIsInRole(Meteor.user(), 'admin')) {
            this.next();
        } else {
            Router.go('/');
        }
    }
};

Router.onBeforeAction(OnBeforeActions.isNotLoggedIn, {
    only: ['listplans', 'addplan', 'editplan', 'listsubscribers', 'myplans']
});

Router.onBeforeAction(OnBeforeActions.isAdmin, {
    only: ['listplans', 'addplan', 'editplan', 'listsubscribers']
});

Router.map(function() {
    this.route('plans', {
        path: '/',
        template: 'plans',
        data: function() {
            return {
                plans: Plans.find()
            }
        }
    });

    this.route('listplans', {
        path: '/admin/plans',
        template: 'listplans',
        data: function() {
            return {
                plans: Plans.find()
            }
        }
    });

    this.route('addplan', {
        path: '/admin/plans/add',
        template: 'addplan'
    });

    this.route('editplan', {
        path: '/admin/plans/edit/:id',
        template: 'editplan',
        data: function() {
            return Plans.findOne({_id: this.params.id})
        }
    });

    this.route('listsubscribers', {
        path: '/admin/subscribers',
        template: 'listsubscribers',
        data: function() {
            return {
                subscribers: Subscribers.find()
            }
        }
    });

    this.route('myplans');
});
