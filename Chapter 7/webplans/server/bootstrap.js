Meteor.startup(function() {
    Meteor.publish('plans', function() {
        return Plans.find();
    });

    Meteor.publish('subscribers', function() {
        return Subscribers.find();
    });

    if (Meteor.users.find().count() == 0) {
        var users = [
            {
                name: "admin",
                email: "bj.peter.delacruz@gmail.com",
                roles: ['admin']
            }
        ]
        _.each(users, function(user) {
            var id = Accounts.createUser({
                email: user.email,
                password: "password",
                profile: {
                    name: user.name
                }
            });
            if (user.roles.length > 0) {
                Roles.addUsersToRoles(id, user.roles);
                console.log("Created " + user.name + " user and added " + user.roles[0] + " role.");
            }
        });
    }
});

Accounts.onCreateUser(function(options, user) {
    var defaultPlan = Plans.findOne({is_default: '1'});
    if (defaultPlan) {
        Subscribers.insert({
            user_id: user._id,
            user_email: user.emails[0].address,
            plan_id: defaultPlan._id,
            plan_name: defaultPlan.plan_name,
            plan_label: defaultPlan.plan_label,
            plan_days: defaultPlan.days,
            plan_price: defaultPlan.price,
            plan_description: defaultPlan.description,
            join_date: new Date()
        });
        if (options.profile) {
            user.profile = options.profile
        }
    }
    return user;
});
