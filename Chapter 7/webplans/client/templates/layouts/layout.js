Template.registerHelper('currentRoute', function(route) {
    return Router.current().route.getName() === route;
});

Template.registerHelper('formatDate', function(date) {
    return moment(date).format('MMM D, YYYY')
});

Template.registerHelper('getEndDate', function(join_date, duration) {
    return moment(join_date).add(duration, 'days').format('MMM D, YYYY');
});

Template.registerHelper('isRegularUser', function() {
    return !Roles.userIsInRole(Meteor.user(), 'admin');
});