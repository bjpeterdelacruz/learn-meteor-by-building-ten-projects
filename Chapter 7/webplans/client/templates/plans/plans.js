Template.plans.events({
    "click .buy-plan": function(event) {
        var plan_id = event.currentTarget.id;
        var plan_name = event.currentTarget.rel;

        var plan_info = Plans.findOne({_id: plan_id});
        Meteor.apply("insertSubscriber", [plan_info, Meteor.userId(), Meteor.user().emails[0].address], function(error, result) {
            if (error) {
                toastr.error("There was a problem trying to join a plan: " + error.reason);
            } else {
                toastr.success("Joined plan!");
            }
        });
    }
});