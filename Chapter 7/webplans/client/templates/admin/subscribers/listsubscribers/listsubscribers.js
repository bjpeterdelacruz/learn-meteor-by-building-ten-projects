Template.listsubscribers.events({
    "click .cancel-subscription": function() {
        if (confirm("Are you sure you want to cancel this plan?")) {
            Meteor.apply("cancelPlan", [this._id], function(error, result) {
                if (error) {
                    toastr.error("There was a problem trying to cancel the plan: " + error.reason);
                } else {
                    toastr.success("Plan cancelled!");
                }
            });
        }
        return false;
    }
});