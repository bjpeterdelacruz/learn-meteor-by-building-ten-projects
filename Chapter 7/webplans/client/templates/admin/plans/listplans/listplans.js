Template.listplans.events({
    "click .delete-plan": function() {
        if (confirm("Are you sure you want to delete this plan?")) {
            Meteor.apply("removePlan", [this._id], function(error, result) {
                if (error) {
                    toastr.error("There was a problem trying to delete the plan: " + error.reason);
                } else {
                    toastr.success("Plan deleted!");
                }
            });
        }
        return false;
    }
});