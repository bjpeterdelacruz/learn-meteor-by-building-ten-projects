Template.addplan.events({
    "submit .add-plan-form": function(event) {
        event.preventDefault();
        var plan_name = event.target.plan_name.value;
        var plan_label = event.target.plan_label.value;
        var days = event.target.days.value;
        var price = event.target.price.value;
        var description = event.target.description.value;
        var is_default = event.target.is_default.value;

        Meteor.apply('insertPlan', [plan_name, plan_label, days, price, description, is_default], function(error, result) {
            if (error) {
                toastr.error("There was a problem trying to add the plan: " + error.reason);
            } else {
                toastr.success("Plan added!");
                Router.go('/admin/plans');
            }
        });

        return false;
    }
});