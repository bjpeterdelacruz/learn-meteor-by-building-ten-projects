Template.editplan.helpers({
    "checkValue": function(value1, value2) {
        return value1 == value2 ? "selected" : "";
    }
});

Template.editplan.events({
    "submit .edit-plan-form": function(event) {
        event.preventDefault();
        var plan_name = event.target.plan_name.value;
        var plan_label = event.target.plan_label.value;
        var days = event.target.days.value;
        var price = event.target.price.value;
        var description = event.target.description.value;
        var is_default = event.target.is_default.value;

        Meteor.apply('updatePlan', [this._id, plan_name, plan_label, days, price, description, is_default], function(error, result) {
            if (error) {
                toastr.error("There was a problem trying to update the plan: " + error.reason);
            } else {
                toastr.success("Plan updated!");
                Router.go('/admin/plans');
            }
        });

        return false;
    }
});