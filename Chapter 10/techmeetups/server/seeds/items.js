Meteor.startup(function() {

  Factory.define('item', Items, {
    name: function() { return Fake.sentence(); },
    rating: function() { return _.random(1, 5); }
  });

  if (Items.find({}).count() === 0) {

    _(10).times(function(n) {
      Factory.create('item');
    });

  }

  if (Types.find({}).count() == 0) {
      Types.insert({name: "Public"});
      Types.insert({name: "Private"});
      Types.insert({name: "Invite Only"});
  }

});
