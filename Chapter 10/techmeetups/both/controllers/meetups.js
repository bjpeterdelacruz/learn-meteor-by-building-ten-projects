MeetupsController = AppController.extend({
  data: {
    meetups: Meetups.find({}),
    types: Types.find({})
  },
  onAfterAction: function() {
    Meta.setTitle('Meetups');
  },
  subscriptions: function() {
    this.subscribe('meetups');
    this.subscribe('types');
  }
});

MeetupsController.events({
    'submit .add-meetup-form': function(event) {
        event.preventDefault();
        var title = event.target.title.value;
        var email = event.target.email.value;
        var topics = event.target.topics.value;
        var type = event.target.type.value;
        var meetupdate = event.target.meetupdate.value;
        var address = event.target.address.value;
        var city = event.target.city.value;
        var stateorprovince = event.target.stateorprovince.value;
        var zipcode = event.target.zipcode.value;
        var country = event.target.country.value;

        var params = {
            title: title,
            email: email,
            topics: topics,
            type: type,
            meetupdate: meetupdate,
            address: address,
            city: city,
            stateorprovince: stateorprovince,
            zipcode: zipcode,
            country: country,
            userId: Meteor.userId(),
            username: Meteor.user().username,
            createdAt: new Date()
        }

        Meteor.call('addMeetup', params, function(error, result) {
            if (error) {
                toastr.error("There was a problem trying to add the meetup: " + error.reason);
            } else {
                toastr.success("Meetup added!");
                Router.go('/meetups');
            }
        });

        return false;
    }
});