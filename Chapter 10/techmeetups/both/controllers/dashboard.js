DashboardController = AppController.extend({
  data: {
    mymeetups: Meetups.find({user: this.userId})
  },
  onAfterAction: function() {
    Meta.setTitle('My Meetups');
  },
  subscriptions: function() {
    this.subscribe('mymeetups');
    this.subscribe('types');
  }
});

DashboardController.events({
    'submit .edit-meetup-form': function(event) {
        event.preventDefault();
        var title = event.target.title.value;
        var email = event.target.email.value;
        var topics = event.target.topics.value;
        var type = event.target.type.value;
        var meetupdate = event.target.meetupdate.value;
        var address = event.target.address.value;
        var city = event.target.city.value;
        var stateorprovince = event.target.stateorprovince.value;
        var zipcode = event.target.zipcode.value;
        var country = event.target.country.value;
        var id = event.target.id.value;

        var params = {
            title: title,
            email: email,
            topics: topics,
            type: type,
            meetupdate: meetupdate,
            address: address,
            city: city,
            stateorprovince: stateorprovince,
            zipcode: zipcode,
            country: country,
            updatedAt: new Date()
        }

        Meteor.call('updateMeetup', id, params, function(error, result) {
            if (error) {
                toastr.error("There was a problem trying to update the meetup: " + error.reason);
            } else {
                toastr.success("Meetup updated!");
                $('#modal' + id).modal('toggle');
            }
        });

        return false;
    },
    'click .remove-meetup': function(event) {
        event.preventDefault();

        if (confirm("Are you sure you want to remove this meetup?")) {
            Meteor.call('removeMeetup', event.currentTarget.id, function(error, result) {
                if (error) {
                    toastr.error("There was a problem trying to remove the event: " + error.reason);
                } else {
                    toastr.success("Meetup successfully removed!");
                }
            });
        }

        return false;
    }
});
