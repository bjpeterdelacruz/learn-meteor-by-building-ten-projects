Template.addMeetup.rendered = function() {
    this.$('.datetimepicker').datetimepicker();
    $.getJSON("http://freegeoip.net/json/", function (data) {
        $('#city').val(data.city);
        $('.states option[value="' + data.region_name + '"]').attr('selected', true);
        $('#zipcode').val(data.zip_code);
        $('.countries option[value="' + data.country_name + '"]').attr('selected', true);
    });
}