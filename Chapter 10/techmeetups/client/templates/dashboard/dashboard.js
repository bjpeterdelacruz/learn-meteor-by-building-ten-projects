Template.dashboard.rendered = function() {
    this.$('.datetimepicker').datetimepicker();
};

// http://stackoverflow.com/a/29900738
Template.dashboard.helpers({
    "isTypeSelected": function(type) {
        return type == Template.parentData(1).type ? 'selected' : '';
    },
    "isStateOrProvinceSelected": function(stateorprovince) {
        return stateorprovince == Template.parentData(1).stateorprovince ? 'selected' : '';
    },
    "isCountrySelected": function(country) {
        return country == Template.parentData(1).country ? 'selected' : '';
    }
});