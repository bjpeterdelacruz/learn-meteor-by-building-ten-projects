Template.registerHelper('getType', function(type) {
    if (type == "Public") {
        return "public";
    } else if (type == "Private") {
        return "private";
    } else if (type == "Invite Only") {
        return "invite-only";
    }
});

Template.registerHelper('getTypes', function() {
    return Types.find({});
});

Template.registerHelper('formatAddressNoZipCode', function(city, stateOrProvince, country) {
    if (stateOrProvince == '--') {
        return city + ', ' + country;
    } else {
        return city + ', ' + stateOrProvince + ', ' + country;
    }
});

Template.registerHelper('formatAddressZipCode', function(city, stateOrProvince, zipCode) {
    if (stateOrProvince == '--') {
        return city + ' ' + zipCode;
    } else {
        return city + ', ' + stateOrProvince + ' ' + zipCode;
    }
});

Template.registerHelper('statesAndProvinces', function() {
    return [{
      "hint": "Alabama",
      "abbrcode": "AL"
    }, {
      "hint": "Alaska",
      "abbrcode": "AK"
    }, {
      "hint": "American Samoa",
      "abbrcode": "AS"
    }, {
      "hint": "Arizona",
      "abbrcode": "AZ"
    }, {
      "hint": "Arkansas",
      "abbrcode": "AR"
    }, {
      "hint": "British Columbia",
      "abbrcode": "BC"
    }, {
      "hint": "California",
      "abbrcode": "CA"
    }, {
      "hint": "Colorado",
      "abbrcode": "CO"
    }, {
      "hint": "Connecticut",
      "abbrcode": "CT"
    }, {
      "hint": "Delaware",
      "abbrcode": "DE"
    }, {
      "hint": "District Of Columbia",
      "abbrcode": "DC"
    }, {
      "hint": "Federated States Of Micronesia",
      "abbrcode": "FM"
    }, {
      "hint": "Florida",
      "abbrcode": "FL"
    }, {
      "hint": "Georgia",
      "abbrcode": "GA"
    }, {
      "hint": "Guam",
      "abbrcode": "GU"
    }, {
      "hint": "Hawaii",
      "abbrcode": "HI"
    }, {
      "hint": "Idaho",
      "abbrcode": "ID"
    }, {
      "hint": "Illinois",
      "abbrcode": "IL"
    }, {
      "hint": "Indiana",
      "abbrcode": "IN"
    }, {
      "hint": "Iowa",
      "abbrcode": "IA"
    }, {
      "hint": "Kansas",
      "abbrcode": "KS"
    }, {
      "hint": "Kentucky",
      "abbrcode": "KY"
    }, {
      "hint": "Louisiana",
      "abbrcode": "LA"
    }, {
      "hint": "Maine",
      "abbrcode": "ME"
    }, {
      "hint": "Manitoba",
      "abbrcode": "MB"
    }, {
      "hint": "Marshall Islands",
      "abbrcode": "MH"
    }, {
      "hint": "Maryland",
      "abbrcode": "MD"
    }, {
      "hint": "Massachusetts",
      "abbrcode": "MA"
    }, {
      "hint": "Michigan",
      "abbrcode": "MI"
    }, {
      "hint": "Minnesota",
      "abbrcode": "MN"
    }, {
      "hint": "Mississippi",
      "abbrcode": "MS"
    }, {
      "hint": "Missouri",
      "abbrcode": "MO"
    }, {
      "hint": "Montana",
      "abbrcode": "MT"
    }, {
      "hint": "Nebraska",
      "abbrcode": "NE"
    }, {
      "hint": "Nevada",
      "abbrcode": "NV"
    }, {
      "hint": "New Brunswick",
      "abbrcode": "NB"
    }, {
      "hint": "New Hampshire",
      "abbrcode": "NH"
    }, {
      "hint": "New Jersey",
      "abbrcode": "NJ"
    }, {
      "hint": "New Mexico",
      "abbrcode": "NM"
    }, {
      "hint": "New York",
      "abbrcode": "NY"
    }, {
      "hint": "Newfoundland and Labrador",
      "abbrcode": "NL"
    }, {
      "hint": "North Carolina",
      "abbrcode": "NC"
    }, {
      "hint": "North Dakota",
      "abbrcode": "ND"
    }, {
      "hint": "Northern Mariana Islands",
      "abbrcode": "MP"
    }, {
      "hint": "Nova Scotia",
      "abbrcode": "NS"
    }, {
      "hint": "Northwest Territories",
      "abbrcode": "NT"
    }, {
      "hint": "Nunavut",
      "abbrcode": "NU"
    }, {
      "hint": "Ohio",
      "abbrcode": "OH"
    }, {
      "hint": "Oklahoma",
      "abbrcode": "OK"
    }, {
      "hint": "Ontario",
      "abbrcode": "ON"
    }, {
      "hint": "Oregon",
      "abbrcode": "OR"
    }, {
      "hint": "Palau",
      "abbrcode": "PW"
    }, {
      "hint": "Pennsylvania",
      "abbrcode": "PA"
    }, {
      "hint": "Prince Edward Island",
      "abbrcode": "PE"
    }, {
      "hint": "Puerto Rico",
      "abbrcode": "PR"
    }, {
      "hint": "Quebec",
      "abbrcode": "QC"
    }, {
      "hint": "Rhode Island",
      "abbrcode": "RI"
    }, {
      "hint": "Saskatchewan",
      "abbrcode": "SK"
    }, {
      "hint": "South Carolina",
      "abbrcode": "SC"
    }, {
      "hint": "South Dakota",
      "abbrcode": "SD"
    }, {
      "hint": "Tennessee",
      "abbrcode": "TN"
    }, {
      "hint": "Texas",
      "abbrcode": "TX"
    }, {
      "hint": "Utah",
      "abbrcode": "UT"
    }, {
      "hint": "Vermont",
      "abbrcode": "VT"
    }, {
      "hint": "Virgin Islands",
      "abbrcode": "VI"
    }, {
      "hint": "Virginia",
      "abbrcode": "VA"
    }, {
      "hint": "Washington",
      "abbrcode": "WA"
    }, {
      "hint": "West Virginia",
      "abbrcode": "WV"
    }, {
      "hint": "Wisconsin",
      "abbrcode": "WI"
    }, {
      "hint": "Wyoming",
      "abbrcode": "WY"
    }, {
      "hint": "Yukon",
      "abbrcode": "YT"
    }]
});