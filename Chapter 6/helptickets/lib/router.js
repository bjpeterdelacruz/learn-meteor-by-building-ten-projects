Router.configure({
    layoutTemplate: 'layout'
});

var OnBeforeActions = {
    isNotLoggedIn: function() {
        if (!Meteor.user()) {
            Router.go('/');
        } else {
            this.next();
        }
    },
    isStaff: function() {
        if (Meteor.user() && Meteor.user().profile.usertype == 'staff') {
            Router.go('/staff');
        } else {
            this.next();
        }
    },
    staffOnly: function() {
        if (Meteor.user() && Meteor.user().profile.usertype == 'staff') {
            this.next();
        } else {
            Router.go('/');
        }
    }
}

Router.onBeforeAction(OnBeforeActions.isNotLoggedIn, {
    only: ['ticket', 'staff', 'departments', 'add_department', 'users']
})

Router.onBeforeAction(OnBeforeActions.isStaff, {
    only: ['mytickets']
})

Router.onBeforeAction(OnBeforeActions.staffOnly, {
    only: ['staff', 'departments' , 'add_department', 'users']
})

Router.map(function() {
    this.route('mytickets', {
        path: '/',
        template: 'mytickets',
        data: function() {
            return {
                tickets: Tickets.find({
                    customer: Meteor.userId(),
                    status: "new"
                }, {
                    sort: {
                        createdAt: -1
                    }
                })
            }
        }
    });

    this.route('ticket', {
        path: '/ticket/:_id',
        template: 'ticket',
        data: function() {
            return Tickets.findOne({_id: this.params._id});
        }
    });

    this.route('staff', {
        path: '/staff',
        template: 'stafftickets',
        data: function() {
            return { tickets: Tickets.find() };
        }
    });

    this.route('departments', {
        path: '/staff/departments',
        template: 'departments',
        data: function() {
            return { departments: Departments.find({}, {
                sort: {
                    department: 1
                }
            }) };
        }
    });

    this.route('add_department', {
        path: '/staff/departments/add',
        template: 'add_department'
    });

    this.route('users', {
        path: '/staff/users',
        template: 'users',
        data: function() {
            return { users: Meteor.users.find({}, {
                fields: {
                    emails: 1,
                    profile: 1
                }
            }) };
        }
    });
});