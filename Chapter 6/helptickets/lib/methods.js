Meteor.methods({
    "createUserAccount": function(first_name, last_name, email, password) {
        Accounts.createUser({
            email: email,
            password: password,
            profile: {
                first_name: first_name,
                last_name: last_name,
                usertype: 'customer'
            }
        });
    },
    "insertTicket": function(name, email, subject, department, priority, message, status, customer) {
        Tickets.insert({
            name: name,
            email: email,
            subject: subject,
            department: department,
            priority: priority,
            message: message,
            status: status,
            customer: customer,
            createdAt: new Date()
        });
    },
    "resolveTicket": function(id) {
        Tickets.update(id, {
            $set: {
                status: "resolved"
            }
        });
    },
    "addReply": function(id, reply, usertype, user) {
        Tickets.update(id, {
            $push: {
                replies: {
                    reply: reply,
                    usertype: usertype,
                    user: user,
                    replyDate: new Date()
                }
            }
        });
    },
    "updateUserAccount": function(id, usertype) {
        var user = Meteor.users.findOne(id);
        Meteor.users.update(id, {
            $set: {
                profile: {
                    first_name: user.profile.first_name,
                    last_name: user.profile.last_name,
                    usertype: usertype
                }
            }
        });
    },
    "addDepartment": function(department, head) {
        Departments.insert({
            department: department,
            head: head
        });
    },
    "removeDepartment": function(id) {
        Departments.remove(id);
    }
});