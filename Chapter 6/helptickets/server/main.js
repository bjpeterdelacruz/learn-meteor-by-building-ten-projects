import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
    Meteor.publish('tickets', function() {
        return Tickets.find();
    });

    Meteor.publish('departments', function() {
        if (Departments.find().count() == 0) {
            Departments.insert({department: "Production", head: "Ron Howard"});
            Departments.insert({department: "Research and Development", head: "Joe Richardson"});
            Departments.insert({department: "Accounting and Finance", head: "Larry Moore"});
        }
        return Departments.find();
    });

    Meteor.publish('priorities', function() {
        if (Priorities.find().count() == 0) {
            Priorities.insert({priority: "low"});
            Priorities.insert({priority: "medium"});
            Priorities.insert({priority: "high"});
        }
        return Priorities.find();
    });

    Meteor.publish('users', function () {
        return Meteor.users.find({}, {
            fields: {
                emails: 1,
                profile: 1
            }
        });
    });
});
