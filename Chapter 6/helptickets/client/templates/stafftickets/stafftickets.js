Template.stafftickets.helpers({
    "isResolved": function(status) {
        return status == 'resolved';
    }
});

Template.stafftickets.events({
    "click .close-ticket": function(event) {
        if (confirm("Are you sure?")) {
            Meteor.apply("resolveTicket", [this._id], function(error, result) {
                if (error) {
                    FlashMessages.sendError("There was a problem trying to close the ticket: " + error.reason);
                } else {
                    FlashMessages.sendSuccess("Successfully closed ticket!");
                    Router.go('/');
                }
            });
        }

        return false;
    }
});