Template.registerHelper('userEmail', function() {
    if (Meteor.user() == null) {
        return '';
    }
    return Meteor.user().emails[0].address;
});

Template.registerHelper('userName', function() {
    if (Meteor.user() == null) {
        return '';
    }
    return Meteor.user().profile.first_name + " " + Meteor.user().profile.last_name;
});

Template.registerHelper('isStaff', function() {
    return Meteor.user().profile.usertype == 'staff';
});

Template.registerHelper('formatDate', function(date) {
    return moment(date).format('MMMM Do YYYY, h:mm:ss A');
});

Template.registerHelper('capitalFirst', function(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
});
