Template.users.events({
    "change .is_staff": function(event) {
        var id = $(event.target).attr("data-id");
        var usertype;
        if ($(event.target).prop("checked")) {
            usertype = 'staff';
        } else {
            usertype = 'customer';
        }

        Meteor.apply("updateUserAccount", [id, usertype], function(error, result) {
            if (error) {
                FlashMessages.sendError("There was a problem trying to update the user: " + error.reason);
            } else {
                FlashMessages.sendSuccess("Successfully updated user!");
            }
        });
    }
});

Template.users.helpers({
    "getName": function(profile) {
        if (profile == null) {
            return '';
        }
        return profile.first_name + " " + profile.last_name;
    },
    "getEmailAddress": function(emails) {
        return emails[0].address;
    },
    "isUserStaff": function(profile) {
        if (profile == null) {
            return '';
        }
        return profile.usertype == 'staff';
    }
});
