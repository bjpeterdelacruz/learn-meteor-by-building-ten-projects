Template.mytickets.helpers({
    departments: function() {
        return Departments.find({}, {sort: {department: 1}});
    },
    priorities: function() {
        return Priorities.find({});
    }
});

Template.mytickets.events({
    "submit .open-ticket-form": function(event) {
        var name = event.target.name.value;
        var email = event.target.email.value;
        var subject = event.target.subject.value;
        var department = event.target.department.value;
        var priority = event.target.priority.value;
        var message = event.target.message.value;
        var status = "new";

        Meteor.apply("insertTicket", [name, email, subject, department, priority, message, status, Meteor.userId()], function(error, result) {
            if (error) {
                FlashMessages.sendError("There was a problem trying to open a new ticket: " + error.reason);
            } else {
                FlashMessages.sendSuccess("Successfully opened new ticket!");
                $('#openTicketModal').modal('hide');
            }
        });

        return false;
    },

    "click .close-ticket": function(event) {
        if (confirm("Are you sure?")) {
            Meteor.apply("resolveTicket", [this._id], function(error, result) {
                if (error) {
                    FlashMessages.sendError("There was a problem trying to close the ticket: " + error.reason);
                } else {
                    FlashMessages.sendSuccess("Successfully closed ticket!");
                    Router.go('/');
                }
            });
        }

        return false;
    }
});