Template.ticket.events({
    "submit .add-reply": function(event) {
        var reply = event.target.reply.value;

        var usertype;
        if (Meteor.user().profile.usertype == 'staff') {
            usertype = 'staff';
        } else {
            usertype = 'customer';
        }

        Meteor.apply("addReply", [this._id, reply, usertype, Meteor.userId()], function(error, result) {
            if (error) {
                FlashMessages.sendError("There was a problem trying to add your reply: " + error.reason);
            } else {
                FlashMessages.sendSuccess("Successfully added reply!");
                event.target.reply.value = '';
            }
        })

        return false;
    }
});