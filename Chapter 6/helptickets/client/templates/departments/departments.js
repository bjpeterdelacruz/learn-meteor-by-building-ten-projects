Template.departments.events({
    "click .delete-department": function(event) {
        if (confirm("Are you sure?")) {
            Meteor.apply("removeDepartment", [this._id], function(error, result) {
                if (error) {
                    FlashMessages.sendError("There was a problem trying to remove the department: " + error.reason);
                } else {
                    FlashMessages.sendSuccess("Successfully removed department!");
                }
            });
        }
    }
});