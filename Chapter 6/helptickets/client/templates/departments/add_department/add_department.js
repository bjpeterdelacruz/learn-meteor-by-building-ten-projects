Template.add_department.events({
    "submit .add-department-form": function(event) {
        var department = event.target.department.value;
        var head = event.target.head.value;

        Meteor.apply("addDepartment", [department, head], function(error, result) {
            if (error) {
                FlashMessages.sendError('There was a problem trying to add a department: ' + error.reason);
            } else {
                FlashMessages.sendSuccess('Successfully added department!');
                Router.go('/staff/departments');
            }
        });

        return false;
    }
});