$().button('toggle');

var redirctAndDisplaySuccessMessage = function(event) {
    event.target.name.value = "";
    event.target.category.value = "";
    event.target.description.value = "";
    event.target.is_featured.value = "";

    Router.go('/');

    FlashMessages.sendSuccess("Product added!");
}

Template.add_product.events({
    "submit .add_product": function(event) {
        var name = event.target.name.value;
        var category = event.target.category.value;
        var description = event.target.description.value;
        var is_featured = event.target.is_featured.value;

        var file = $('#productImage').get(0).files[0];

        if (file) {
            ProductsImages.insert(new FS.File(file), function(error, result) {
                if (error) {
                    throw new Meteor.Error(error);
                } else {
                    // See http://stackoverflow.com/a/29832414/3640269
                    // There are no callbacks on the client side yet, so a timer will have to do for now.
                    var intervalHandle = Meteor.setInterval(function() {
                        if (result.hasStored("ProductsImages")) {
                            // The image has been stored on the server, so now it is safe to give the client the URL to it.
                            var imageLoc = '/cfs/files/ProductsImages/' + result._id;

                            Meteor.apply('insertProductImage', [name, category, description, is_featured, imageLoc], function(error, result) {
                                redirctAndDisplaySuccessMessage(event);
                            });

                            Meteor.clearInterval(intervalHandle);
                        }
                    }, 100);
                }
            });
        } else {
            Meteor.apply('insertProductImage', [name, category, description, is_featured, "/img/noimage.png"], function(error, result) {
                redirctAndDisplaySuccessMessage(event);
            });
        }

        return false;
    }
});