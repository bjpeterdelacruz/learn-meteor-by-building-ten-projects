Template.new_review.events({
    "submit .new-review": function(event) {
        var id = this._id;
        var rating = event.target.rating.value;
        var body = event.target.body.value;

        Meteor.apply('addProductReview', [id, rating, body], function(error, result) {
            FlashMessages.sendSuccess("Review added!");
            Router.go('/products/' + id);
        });

        return false;
    }
});