Template.registerHelper('truncateText', function(text, length) {
    if (text.length < length) {
        return text;
    }
    var newText = text.substring(0, length);
    newText = newText.substr(0, Math.min(newText.length, newText.lastIndexOf(" ")));
    return new Spacebars.SafeString(newText + "...");
});

Template.registerHelper('getAverage', function(array) {
    if (array == null || array.length == 0) {
        return 0;
    }
    var sum = 0;
    for (var i = 0; i < array.length; i++) {
        sum += parseInt(array[i].rating, 10);
    }
    return Math.round(sum / array.length);
});

Template.registerHelper('getTotal', function(array) {
    if (array == null || array.length == 0) {
        return 0;
    }
    return array.length;
});

Template.registerHelper('formatDate', function(date) {
  return moment(date).format('MMMM Do, YYYY, h:mm A');
});