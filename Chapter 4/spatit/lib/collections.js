Categories = new Mongo.Collection("categories");

Products = new Mongo.Collection("products");

ProductsImages = new FS.Collection("ProductsImages", {
    stores: [new FS.Store.GridFS("ProductsImages")]
});

ProductsImages.allow({
    insert: function(fileId, document) {
        return true;
    },
    download: function(fileId, document) {
        return true;
    },
    update: function(fileId, document) {
        return true;
    }
});

Meteor.methods({
    insertProductImage: function(name, category, description, is_featured, imageLoc) {
        Products.insert({
            name: name,
            category: category,
            description: description,
            is_featured: is_featured,
            image: imageLoc,
            createdAt: new Date()
        });
    },

    addProductReview: function(id, rating, body) {
        Products.update({
            _id: id
        }, {
            $push: {
                reviews: {
                    rating: rating,
                    body: body,
                    review_date: new Date()
                }
            }
        });
    }
});

Products.allow({
    insert: function(fileId, document) {
        return true;
    },
    update: function(fileId, document) {
        return true;
    }
});