Router.configure({
    layoutTemplate: 'layout'
});

var OnBeforeActions = {
    loginRequired: function() {
        if (!Meteor.userId()) {
            Router.go('/');
        } else {
            this.next();
        }
    }
}

Router.onBeforeAction(OnBeforeActions.loginRequired, {
    only: ['add_product', 'new_review']
});

Router.map(function() {
    this.route('home', {
        path: '/',
        template: 'home',
        data: function() {
            return {
                products: Products.find({is_featured: "1"}, {
                    sort: {
                        createdAt: -1
                    }
                })
            };
        }
    });

    this.route('products', {
        path: '/products',
        template: 'products',
        data: function() {
            return {
                products: Products.find({}, {
                    sort: {
                        createdAt: -1
                    }
                })
            };
        }
    });

    this.route('add_product', {
        path: '/add_product',
        template: 'add_product',
        data: function() {
            return {
                categories: Categories.find({}, {
                    sort: {
                        name: 1
                    }
                })
            };
        }
    });

    this.route('category_products', {
        path: '/categories/:slug',
        template: 'category_products',
        data: function() {
            return {
                category_products: Products.find({category: this.params.slug}, {
                    sort: {
                        createdAt: -1
                    }
                })
            };
        }
    });

    this.route('new_review', {
        path: '/new-review/:_id',
        template: 'new_review',
        data: function() {
            return Products.findOne(this.params._id)
        }
    });

    this.route('product', {
        path: '/products/:_id',
        template: 'product',
        data: function() {
            return Products.findOne(this.params._id)
        }
    });
});