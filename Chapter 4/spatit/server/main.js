import { Meteor } from 'meteor/meteor';

Meteor.publish('categories', function() {
    return Categories.find();
});

Meteor.publish('products', function() {
    return Products.find();
});

Meteor.publish('ProductsImages', function() {
    return ProductsImages.find();
});

Meteor.startup(() => {
  
});
