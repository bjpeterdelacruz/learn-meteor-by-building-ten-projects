Meteor.methods({
    "createPost": function(title, body) {
        Posts.insert({
            title: title,
            body: body
        });
    },
    "updatePost": function(id, title, body) {
        Posts.update({_id: id}, {
            $set: {
                title: title,
                body: body
            }
        });
    },
    "deletePost": function(id) {
        Posts.remove(id);
    },
    "addProject": function(name, projectDate, client, type, description, imageLoc, imageId) {
        Projects.insert({
            name: name,
            projectDate: projectDate,
            client: client,
            type: type,
            description: description,
            projectImage: imageLoc,
            imageId: imageId
        });
    },
    "updateProject": function(id, name, projectDate, client, type, description, imageLoc, imageId, oldImageId) {
        if (oldImageId) {
            // Remove old project image
            ProjectImages.remove(oldImageId);
        }
        Projects.update({_id: id}, {
            $set: {
                name: name,
                projectDate: projectDate,
                client: client,
                type: type,
                description: description,
                projectImage: imageLoc,
                imageId: imageId
            }
        });
    },
    "deleteProject": function(id) {
        var project = Projects.findOne(id);
        if (project) {
            Projects.remove(id);
            if (project.imageId) {
                // Remove project image as well
                ProjectImages.remove(project.imageId);
            }
        }
    }
});