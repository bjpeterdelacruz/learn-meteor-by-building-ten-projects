Router.configure({
    layoutTemplate: 'layout'
});

var OnBeforeActions = {
    isNotLoggedIn: function() {
        if (!Meteor.user()) {
            Router.go('/admin');
        } else {
            this.next();
        }
    }
};

Router.onBeforeAction(OnBeforeActions.isNotLoggedIn, {
    only: ['list_posts', 'add_post', 'edit_post', 'list_projects', 'add_project', 'edit_project', 'change_password']
});

Router.map(function() {
    this.route('home', {
        path: '/',
        template: 'home'
    });

    this.route('work', {
        path: '/work',
        template: 'work',
        data: function() {
            return {
                projects: Projects.find()
            }
        }
    });

    this.route('about');

    this.route('blog', {
        path: '/blog',
        template: 'blog',
        data: function() {
            return {
                posts: Posts.find()
            }
        }
    });

    this.route('blog_post', {
        path: '/blog/post/:_id',
        template: 'blog_post',
        data: function() {
            return Posts.findOne(this.params._id);
        }
    });

    this.route('project', {
        path: '/project/:_id',
        template: 'project',
        data: function() {
            return Projects.findOne(this.params._id);
        }
    })

    this.route('contact');

    this.route('list_posts', {
        path: '/admin/posts',
        template: 'list_posts',
        data: function() {
            return {
                posts: Posts.find()
            }
        }
    });

    this.route('add_post', {
        path: '/admin/posts/add',
        template: 'add_post'
    });

    this.route('edit_post', {
        path: '/admin/posts/:_id/edit',
        template: 'edit_post',
        data: function() {
            return Posts.findOne(this.params._id)
        }
    });

    this.route('list_projects', {
        path: '/admin/projects',
        template: 'list_projects',
        data: function() {
            return {
                projects: Projects.find()
            }
        }
    });

    this.route('add_project', {
        path: '/admin/projects/add',
        template: 'add_project'
    });

    this.route('edit_project', {
        path: '/admin/projects/:_id/edit',
        template: 'edit_project',
        data: function() {
            return Projects.findOne(this.params._id)
        }
    });

    this.route('login', {
        path: '/admin',
        template: 'login'
    });

    this.route('change_password', {
        path: '/admin/change_password',
        template: 'change_password'
    });
});