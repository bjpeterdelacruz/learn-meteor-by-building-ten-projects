Posts = new Mongo.Collection('posts');

Posts.attachSchema(new SimpleSchema({
    title: {
        type: String,
        max: 100
    },
    body: {
        type: String
    },
    userId: {
        type: String,
        autoValue: function() {
            return Meteor.userId()
        }
    },
    updatedAt: {
        type: Date,
        autoValue: function() {
            return new Date()
        }
    }
}));

Projects = new Mongo.Collection('projects');

Projects.attachSchema(new SimpleSchema({
    name: {
        type: String,
        max: 100
    },
    projectDate: {
        type: String,
        max: 100,
        optional: true
    },
    client: {
        type: String,
        max: 100
    },
    type: {
        type: String,
        max: 100
    },
    description: {
        type: String
    },
    projectImage: {
        type: String,
        max: 100,
        optional: true
    },
    userId: {
        type: String,
        autoValue: function() {
            return Meteor.userId()
        }
    },
    updatedAt: {
        type: Date,
        autoValue: function() {
            return new Date()
        }
    },
    imageId: {
        type: String,
        optional: true
    }
}));

ProjectImages = new FS.Collection('ProjectImages', {
    stores: [new FS.Store.GridFS('ProjectImages')]
});

ProjectImages.allow({
    insert: function(userId, doc) {
        return true;
    },
    update: function(userId, doc, fields, modifier) {
        return true;
    },
    download: function() {
        return true;
    }
});