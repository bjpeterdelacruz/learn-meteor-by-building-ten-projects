import { Meteor } from 'meteor/meteor';

Meteor.startup(function() {
    Meteor.publish('posts', function() {
        return Posts.find();
    });

    Meteor.publish('projects', function() {
        return Projects.find();
    });

    Meteor.publish('ProjectImages', function() {
        return ProjectImages.find();
    });

    if (Meteor.users.find().count() == 0) {
        Accounts.createUser({
            username: 'admin',
            email: 'bj.peter.delacruz@gmail.com',
            password: 'password'
        })
    }
});
