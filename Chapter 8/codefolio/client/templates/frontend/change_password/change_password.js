Template.change_password.events({
    "submit .change-password": function(event) {
        event.preventDefault();
        var oldPassword = event.target.oldPassword.value;
        var newPassword = event.target.newPassword.value;
        var confirmPassword = event.target.confirmPassword.value;

        if (oldPassword === newPassword) {
            FlashMessages.sendError("New password must be different from old password.");
            return false;
        }

        var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
        if (!regularExpression.test(newPassword)) {
            FlashMessages.sendError("Password must be between 8-16 characters long and contain at least one number and at least one symbol.");
            return false;
        }

        if (newPassword != confirmPassword) {
            FlashMessages.sendError("Passwords do not match. Please try again.");
            return false;
        }

        Accounts.changePassword(oldPassword, newPassword, function(error) {
            if (error) {
                event.target.oldPassword.value = oldPassword;
                event.target.newPassword.value = newPassword;
                event.target.confirmPassword.value = confirmPassword;
                FlashMessages.sendError("There was a problem trying to change your password: " + error.reason);
            } else {
                FlashMessages.sendSuccess("Password successfully changed!");
                Router.go('/admin/projects');
            }
        });

        return false;
    }
});