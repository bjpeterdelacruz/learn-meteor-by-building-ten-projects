Template.login.events({
    "submit .login-user": function(event) {
        event.preventDefault();
        var username = event.target.username.value;
        var password = event.target.password.value;

        Meteor.loginWithPassword(username, password, function(error) {
            if (error) {
                event.target.username.value = username;
                event.target.password.value = password;
                FlashMessages.sendError("There was a problem trying to log you in: " + error.reason);
            } else {
                if (password == "password") {
                    FlashMessages.sendWarning("Please change your password.");
                    Router.go('/admin/change_password');
                } else {
                    FlashMessages.sendSuccess("You are now logged in!");
                    Router.go('/admin/projects');
                }
            }
        });

        return false;
    }
});