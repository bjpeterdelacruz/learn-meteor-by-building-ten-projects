Template.layout.events({
    "click .logout-user": function(event) {
        event.preventDefault();
        Meteor.logout(function(error) {
            if (error) {
                FlashMessages.sendError("There was a problem trying to log you out: " + error.reason);
            } else {
                FlashMessages.sendSuccess("You are now logged out!");
                Router.go('/');
            }
        })

        return false;
    }
});

Template.registerHelper('formatDate', function(date) {
    return moment(date).format('MMMM Do, YYYY, h:mm:ss A');
});

Template.registerHelper('getSiteTitle', function() {
    return "CodeFolio";
});

Template.registerHelper('getAdminName', function() {
    return "BJ Peter DeLaCruz";
});

Template.registerHelper('getAdminImage', function() {
    return "/assets/img/user.png";
});

Template.registerHelper('truncateText', function(text, length) {
    if (text.length < length) {
        return text;
    }
    var newText = text.substring(0, length);
    newText = newText.substr(0, Math.min(newText.length, newText.lastIndexOf(" ")));
    return new Spacebars.SafeString(newText + "...");
});