Template.list_projects.events({
    "click .delete_project": function(event) {
        event.preventDefault();
        if (confirm("Are you sure you want to delete this project?")) {
            Meteor.apply("deleteProject", [this._id], function(error, result) {
                if (error) {
                    FlashMessages.sendError("There was a problem trying to delete the project: " + error.reason);
                } else {
                    FlashMessages.sendSuccess("Project was deleted successfully!");
                }
            });
        }
        return false;
    }
});