Template.add_project.onRendered(function() {
    this.$('.datetimepicker').datetimepicker();
});

Template.add_project.events({
    "submit .add_project_form": function(event) {
        event.preventDefault();

        var name = event.target.name.value;
        var projectDate = event.target.projectDate.value;
        var client = event.target.client.value;
        var type = event.target.type.value;
        var description = event.target.description.value;

        var file = $('#projectImage').get(0).files[0];

        if (file) {
            ProjectImages.insert(new FS.File(file), function(error, result) {
                if (error) {
                    throw new Meteor.Error(error);
                } else {
                    // See http://stackoverflow.com/a/29832414/3640269
                    // There are no callbacks on the client side yet, so a timer will have to do for now.
                    var intervalHandle = Meteor.setInterval(function() {
                        if (result.hasStored("ProjectImages")) {
                            // The image has been stored on the server, so now it is safe to give the client the URL to it.
                            var imageLoc = '/cfs/files/ProjectImages/' + result._id;

                            Meteor.apply('addProject', [name, projectDate, client, type, description, imageLoc, result._id], function(error, result) {
                                if (error) {
                                    FlashMessages.sendError("There was a problem trying to add the project: " + error.reason);
                                } else {
                                    FlashMessages.sendSuccess("Successfully added project!");
                                    Router.go('/admin/projects');
                                }
                            });

                            Meteor.clearInterval(intervalHandle);
                        }
                    }, 100);
                }
            });
        } else {
            Meteor.apply('addProject', [name, projectDate, client, type, description, "/assets/img/noimage.svg"], function(error, result) {
                if (error) {
                    FlashMessages.sendError("There was a problem trying to add the project: " + error.reason);
                } else {
                    FlashMessages.sendSuccess("Successfully added project!");
                    Router.go('/admin/projects');
                }
            });
        }

        return false;
    }
});