Template.edit_project.events({
    "submit .edit_project_form": function(event) {
        event.preventDefault();

        var id = this._id;
        var name = event.target.name.value;
        var projectDate = event.target.projectDate.value;
        var client = event.target.client.value;
        var type = event.target.type.value;
        var description = event.target.description.value;
        var imageId = event.target.imageId.value;

        var file = $('#projectImage').get(0).files[0];

        if (file) {
            ProjectImages.insert(new FS.File(file), function(error, result) {
                if (error) {
                    throw new Meteor.Error(error);
                } else {
                    // See http://stackoverflow.com/a/29832414/3640269
                    // There are no callbacks on the client side yet, so a timer will have to do for now.
                    var intervalHandle = Meteor.setInterval(function() {
                        if (result.hasStored("ProjectImages")) {
                            // The image has been stored on the server, so now it is safe to give the client the URL to it.
                            var imageLoc = '/cfs/files/ProjectImages/' + result._id;

                            Meteor.apply('updateProject', [id, name, projectDate, client, type, description, imageLoc, result._id, imageId], function(error, result) {
                                if (error) {
                                    FlashMessages.sendError("There was a problem trying to update the project: " + error.reason);
                                } else {
                                    FlashMessages.sendSuccess("Successfully updated project!");
                                    Router.go('/admin/projects');
                                }
                            });

                            Meteor.clearInterval(intervalHandle);
                        }
                    }, 100);
                }
            });
        } else {
            var imageLoc = "/assets/img/noimage.svg";
            if (imageId) {
                imageLoc = "/cfs/files/ProjectImages/" + imageId;
            }
            Meteor.apply('updateProject', [id, name, projectDate, client, type, description, imageLoc], function(error, result) {
                if (error) {
                    FlashMessages.sendError("There was a problem trying to update the project: " + error.reason);
                } else {
                    FlashMessages.sendSuccess("Successfully updated project!");
                    Router.go('/admin/projects');
                }
            });
        }

        return false;
    }
});