Template.add_post.events({
    "submit .add_post_form": function(event) {
        event.preventDefault();
        
        var title = event.target.title.value;
        var body = event.target.body.value;
        
        Meteor.apply("createPost", [title, body], function(error, result) {
            if (error) {
                FlashMessages.sendError("There was a problem trying to create the post: " + error.reason);
            } else {
                FlashMessages.sendSuccess("Post was created successfully!");
                Router.go('/admin/posts');
            }
        });
        
        return false;
    }
});