Template.edit_post.events({
    "submit .edit_post_form": function(event) {
        event.preventDefault();
        
        var title = event.target.title.value;
        var body = event.target.body.value;
        
        Meteor.apply("updatePost", [this._id, title, body], function(error, result) {
            if (error) {
                FlashMessages.sendError("There was a problem trying to update the post: " + error.reason);
            } else {
                FlashMessages.sendSuccess("Post was updated successfully!");
                Router.go('/admin/posts');
            }
        });
        
        return false;
    }
});