Template.list_posts.events({
    "click .delete_post": function(event) {
        event.preventDefault();
        if (confirm("Are you sure you want to delete this post?")) {
            Meteor.apply("deletePost", [this._id], function(error, result) {
                if (error) {
                    FlashMessages.sendError("There was a problem trying to delete the post: " + error.reason);
                } else {
                    FlashMessages.sendSuccess("Post was deleted successfully!");
                }
            });
        }
        return false;
    }
});