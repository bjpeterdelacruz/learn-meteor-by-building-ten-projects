Meteor.startup(function() {
    Meteor.subscribe('posts');

    Meteor.subscribe('projects');

    Meteor.subscribe('ProjectImages');

    Accounts.ui.config({
        passwordSignupFields: 'USERNAME_ONLY'
    });
});