import { Meteor } from 'meteor/meteor';

import '../common/collections.js';
import '../common/methods.js';

Meteor.startup(() => {
  // code to run on server at startup
  Meteor.publish('todos', function() {
      if (!this.userId) {
          return Todos.find({});
      } else {
          return Todos.find({userId: this.userId});
      }
  });
});
