Images = new FS.Collection('Images', {
    stores: [new FS.Store.GridFS('Images')],
    filter: {
        allow: {
            contentTypes: ['image/*']
        },
        onInvalid: function(message) {
            FlashMessages.sendError(message);
        }
    }
});

Images.allow({
    insert: function() {
        return true;
    },
    update: function() {
        return true;
    },
    download: function() {
        return true;
    }
});

ImageInfos = new Mongo.Collection('ImageInfos');

Permissions = new Mongo.Collection('Permissions');