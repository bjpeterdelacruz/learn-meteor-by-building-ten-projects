Meteor.methods({
    "insertImageInfo": function(imageId, title, story, userId, displayName, screenName, permission) {
        ImageInfos.insert({
            imageId: imageId,
            title: title,
            story: story,
            imageUrl: '/cfs/files/Images/' + imageId,
            userId: userId,
            displayName: displayName,
            screenName: screenName,
            createdAt: new Date(),
            permission: permission
        });
    },
    "removeImage": function(imageId) {
        Images.remove(imageId);
        ImageInfos.remove(ImageInfos.findOne({imageId: imageId})._id);
    }
})