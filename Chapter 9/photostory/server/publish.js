import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  Meteor.publish('Images', function() {
      return Images.find();
  });

  Meteor.publish('ImageInfos', function() {
      return ImageInfos.find();
  });

  if (Permissions.find().count() == 0) {
      Permissions.insert({name: "Public", value: "public"});
      Permissions.insert({name: "Private", value: "private"});
  }

  Meteor.publish('Permissions', function() {
      return Permissions.find();
  });

  Meteor.publish("userData", function () {
      return Meteor.users.find({_id: this.userId}, {
          fields: {'services.twitter.screenName': 1}
      });
  });
});
