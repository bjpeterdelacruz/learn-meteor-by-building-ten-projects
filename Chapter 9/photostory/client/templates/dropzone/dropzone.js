Template.dropzone.events({
    "dropped #dropzone": function(event) {
        FS.Utility.eachFile(event, function(file) {
            Images.insert(new FS.File(file), function(error, result) {
                if (error) {
                    FlashMessages.sendError("There was a problem trying to upload the file: " + error.reason);
                } else {
                    FlashMessages.sendSuccess("File uploaded successfully!");
                    Session.set('imageId', result._id);
                    Modal.show('addInfo', {permissions: Permissions.find()});
                }
            });
        });
    }
});