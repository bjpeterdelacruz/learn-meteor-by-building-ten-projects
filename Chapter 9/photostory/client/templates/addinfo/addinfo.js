Template.addInfo.events({
    'submit .add-image-info': function(event) {
        var userId = Meteor.userId();
        if (!userId) {
            throw new Meteor.Error("Not authorized.");
        }
        event.preventDefault();
        var imageId = Session.get('imageId');
        var title = event.target.title.value;
        var story = event.target.story.value;
        var permission = $('#permission option:selected').text();
        var user = Meteor.user();

        Meteor.apply('insertImageInfo', [imageId, title, story, userId, user.profile.name, user.services.twitter.screenName, permission], function(error, result) {
            if (error) {
                FlashMessages.sendError("There was a problem trying to insert the image information: " + error.reason);
            } else {
                FlashMessages.sendSuccess("Successfully saved image information!");
                Modal.hide('addInfo');
            }
        });

        return false;
    }
});