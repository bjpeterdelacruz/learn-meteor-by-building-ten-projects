Template.home.helpers({
    images: function() {
        var imageIds;
        if (Meteor.user()) {
            imageIds = ImageInfos.find({
                $or: [
                    {
                        permission: "Public"
                    },
                    {
                        permission: "Private",
                        userId: Meteor.userId()
                    }
                ]
            }).map(function(image_info) {
                return image_info.imageId;
            });
        } else {
            imageIds = ImageInfos.find({permission: "Public"}).map(function(image_info) {
                return image_info.imageId;
            });
        }
        return Images.find({_id: {$in: imageIds}}, {sort: {uploadedAt: -1}});
    }
});