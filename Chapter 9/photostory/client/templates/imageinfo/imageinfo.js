Template.imageInfo.helpers({
    getTitle: function(imageId) {
        return ImageInfos.findOne({imageId: imageId}).title;
    },
    getStory: function(imageId) {
        return ImageInfos.findOne({imageId: imageId}).story;
    },
    getDisplayName: function(imageId) {
        return ImageInfos.findOne({imageId: imageId}).displayName;
    },
    getScreenName: function(imageId) {
        return ImageInfos.findOne({imageId: imageId}).screenName;
    }
});