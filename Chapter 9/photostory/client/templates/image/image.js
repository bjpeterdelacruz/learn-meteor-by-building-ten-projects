Template.image.events({
    "click .remove-image": function(event) {
        if (!Meteor.userId()) {
            throw new Meteor.Error("Not authorized.");
        }
        event.preventDefault();
        if (confirm("Are you sure you want to delete this image?")) {
            Meteor.apply("removeImage", [this._id], function(error, result) {
                if (error) {
                    FlashMessages.sendError("There was a problem trying to delete the image: " + error.reason);
                } else {
                    FlashMessages.sendSuccess("Deleted image successfully!");
                }
            });
        }
        return false;
    }
});

Template.image.helpers({
    "isOwn": function(imageId) {
        return ImageInfos.findOne({imageId: imageId}).userId == Meteor.userId();
    }
});