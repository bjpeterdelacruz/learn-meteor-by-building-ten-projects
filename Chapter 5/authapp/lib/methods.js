Meteor.methods({
    "createUserAccount": function(first_name, last_name, email, password) {
        Accounts.createUser({
            email: email,
            password: password,
            profile: {
                first_name: first_name,
                last_name: last_name
            }
        });
    }
});