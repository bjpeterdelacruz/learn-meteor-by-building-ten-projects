Template.register.events({
    "submit .form-signup": function(event) {
        event.preventDefault();
        var first_name = trimInput(event.target.first_name.value);
        var last_name = trimInput(event.target.last_name.value);
        var email = trimInput(event.target.email.value);
        var password = trimInput(event.target.password.value);
        var confirm_password = trimInput(event.target.confirm_password.value);

        if (isEmpty(first_name)) {
            return false;
        }
        if (isEmpty(last_name)) {
            return false;
        }
        if (isEmpty(email)) {
            return false;
        }
        if (!isEmail(email)) {
            return false;
        }
        if (!areValidPasswords(password, confirm_password)) {
            return false;
        }

        Meteor.call('createUserAccount', first_name, last_name, email, password, function(error) {
            if (error) {
                FlashMessages.sendError('There was an error with registration: ' + error);
            } else {
                FlashMessages.sendSuccess('Account created!');
                Meteor.loginWithPassword(email, password, function(err) {
                    if (err) {
                        FlashMessages.sendError('There was a problem logging you in: ' + err.reason);
                    } else {
                        FlashMessages.sendSuccess('You are now logged in.');
                        Router.go('/dashboard');
                    }
                });
            }
        });

        return false;
    }
});

var trimInput = function(value) {
    return value.replace(/^\s*|\s*$/g, "");
};

var isEmpty = function(value) {
    if (value == '') {
        FlashMessages.sendError('Please fill out all fields.');
        return true;
    }
    return false;
};

var isEmail = function(value) {
    var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (filter.test(value)) {
        return true;
    }
    FlashMessages.sendError('Please use a valid e-mail address.');
    return false;
};

var isValidPassword = function(password) {
    if (password.length < 6) {
        FlashMessages.sendError('Password must be at least 6 characters long.');
        return false;
    }
    return true;
};

var areValidPasswords = function(password, confirm) {
    if (!isValidPassword(password)) {
        return false;
    }
    if (password !== confirm) {
        FlashMessages.sendError('Passwords do not match.');
        return false;
    }
    return true;
}