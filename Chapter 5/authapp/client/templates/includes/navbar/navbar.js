Template.navbar.events({
    "click .logout-btn": function(event) {
        Meteor.logout(function(error) {
            if (error) {
                FlashMessages.sendError('There was a problem logging you out: ' + error.reason);
            } else {
                FlashMessages.sendSuccess('Logout successful!');
                Router.go('/');
            }
        })
    }
});