Template.profile.events({
    "submit .edit-profile": function(event) {
        var file = $('#profileImage').get(0).files[0];

        if (file) {
            ProfileImages.insert(new FS.File(file), function(error, result) {
                if (error) {
                    throw new Meteor.Error(error);
                } else {
                    // See http://stackoverflow.com/a/29832414/3640269
                    // There are no callbacks on the client side yet, so a timer will have to do for now.
                    var intervalHandle = Meteor.setInterval(function() {
                        if (result.hasStored("ProfileImages")) {
                            // The image has been stored on the server, so now it is safe to give the client the URL to it.
                            var imageLoc = '/cfs/files/ProfileImages/' + result._id;

                            var userImage = UserImages.findOne({userId: Meteor.userId()});
                            if (userImage) {
                                Meteor.apply('updateUserImage', [userImage._id, imageLoc], function(error, result) {
                                    Router.go('/');
                                });
                            } else {
                                Meteor.apply('insertUserImage', [imageLoc], function(error, result) {
                                    Router.go('/');
                                });
                            }

                            Meteor.clearInterval(intervalHandle);
                        }
                    }, 100);
                }
            });
        }

        return false;
    }
});