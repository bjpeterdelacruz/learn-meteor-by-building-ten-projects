# Learn Meteor by Building Ten Projects

## About

This repository contains source code for ten different Meteor projects.

## Project Descriptions

**Chapter 1: myTodos**

After they register and log in, users can add todos to a list. They can either cross out or delete those they have finished.

> __Live:__ [http://tinyurl.com/mytodos2](http://tinyurl.com/mytodos2)

**Chapter 2: MicroPosts**

Registered users can post brief posts (microposts) that everyone can read. They can also change their profile picture.

> __Live:__ [http://tinyurl.com/microposts](http://tinyurl.com/microposts)

**Chapter 3: FAQ Component**

Users can view a list of frequently asked questions (FAQs) that were added by an administrator. The admin panel was implemented using [Houston](https://github.com/gterrono/houston).

> __Live:__ [http://tinyurl.com/faq-component](http://tinyurl.com/faq-component)

**Chapter 4: SpatIt!**

Registered users can add products as well as rate and review products.

> __Live:__ [http://tinyurl.com/spatit](http://tinyurl.com/spatit)

**Chapter 5: AuthApp**

A login system that was built from the ground up. The system can be integrated with other Meteor applications.

> __Live:__ [http://tinyurl.com/authapp](http://tinyurl.com/authapp)

**Chapter 6: HelpTickets**

Registered users (customers) can submit tickets. Both customers and staff members can comment on tickets as well as close tickets.

> __Live:__ [http://tinyurl.com/helptickets](http://tinyurl.com/helptickets)

**Chapter 7: WebPlans**

Registered users can subscribe to a plan. They are automatically enrolled in one after they register. A payment system has **not** been implemented yet.

> __Live:__ [http://webplans.herokuapp.com](http://webplans.herokuapp.com)

**Chapter 8: CodeFolio**

This web application contains the owner's blog as well as information about his or her projects. When the owner logs in for the first time, he or she will be asked to change his or her password. The [Stanley](http://blacktie.co/2014/01/stanley-freelancer-theme) theme was used to develop the frontend.

> __Live:__ [http://tinyurl.com/codefolio](http://tinyurl.com/codefolio)

**Chapter 9: PhotoStory**

After signing in with their Twitter account, users can post images and add information about them. Images can be made public (visible to everyone) or private (only visible to the user).

> __Live:__ [https://tinyurl.com/photostory-twitter](https://tinyurl.com/photostory-twitter)

**Chapter 10: TechMeetups**

This application is based on [meteor-boilerplate](https://bitbucket.org/bjpeterdelacruz/meteor-boilerplate). After they register, users can add meetups or browse the directory for meetups.

>__Live:__ [http://tinyurl.com/techmeetups](http://tinyurl.com/techmeetups)